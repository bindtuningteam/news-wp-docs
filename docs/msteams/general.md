![options](../images/modern/07.options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Create New BT News List](./createlist)
- [List Settings](./list)
- [Layout Options](./layout)
- [Content Options](./content)
- [Category Settings](./category)
- [Web Part Appearance](./appearance)
- [Performance](./performance)
- [Advanced Options](./advanced)
- [Web Part Messages](./message)