1. Open the Team on **Teams panel** that you intend to edit an item of the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/settings_edit.png)
2. On the web part sidebar and click the **Manage News** icon;

	![manage_maps.png](../../images/msteams/manage_news.png)

3. The list of News will appear. Click the **Edit** icon to edit the Item;
4. You can check what you need to do in each section on the [Article Preview](../../global/preview);
5. Done editing? Click on **Save Changes** to save your settings. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.

	![edit_map.png](../../images/msteams/news_edit.png)