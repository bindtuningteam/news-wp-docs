1. Open the Team on **Teams panel** that you intend to add an new item to the Web Part; 
2. Click on the **Settings** button. 

	![settings_delete.png](../../images/msteams/settings_edit.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **News**;
3. Fill out the form that pops up. You can check what you need to do in each section on the [Article Preview](../../global/preview);

4. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more markers with similar configuration. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.

	![Add_new_news_teams.gif](../../images/msteams/add_new_news_teams.gif)