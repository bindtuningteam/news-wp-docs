1. Open the Team on **Teams panel** that you intend to remove an item of the Web Part;
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/settings_edit.png)

2. On the web part click the **Manage News** icon;

    ![manage_maps.png](../../images/msteams/manage_news.png)

3. The list of News will appear. Click the **trash** icon to delete the News item;

	![delete_map.png](../../images/msteams/news_edit.png)

4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the News will be removed.