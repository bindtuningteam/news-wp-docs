![open_list_settings.png](../images/msteams/manage_news_list.gif)

### List Settings

Here you will need to connect your web part with a SharePoint list. Without it, the web part will not function and an notification message will appear. Let's get to it:

On **List Settings** you will need to set the lists you want to retrieve your users from.

1. On the first field, place the link to a SharePoint site or list. On the dropdown to the side, you can select a list or one will be selected for you automatically.
2. On **Filtering Options** section, select how you want to filter your items:

    - **No Filters** All the items from your list will be retrieved and displayed on the web part.
    - **CAML Query**: This advanced option gives you full control over how you filter your results. Selecting this option, a text field appears to write your own CAML Query. We recommend the use of the free to use tool <a href="http://www.u2u.be/software/" target="_blank">U2U Caml Query Builder</a>, to help write CAML Queries.
    - **Other Options** : Under the first 2 options, your list views will be listed.

3. On the **Field Mappings** section, map the fields from the list with the fields of the web part:

    - You will find dropdowns for each of the web part's fields. These dropdowns include all the columns in your list's selected view. Select which column corresponds to which field or select **No Mapping** when nothing fits the bill. If you want to repeat the same column for different fields that's ok too.

    ![addanapp.png](../images/classic/25.mappings-card.png)

4. When you're done, lock your selection by clicking the save icon.

____
### How to use CAML Query

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>

<p class="alert alert-info">Use relative paths for this field. So instead of using an URL like <b>https://company.sharepoint.com/sites/Home</b>, you should use something like <b>/sites/Home/</b>. This will ensure that the web part will work regardless of how you’re accessing the site.</p>

____
### Item Sorting Options

<p class="alert alert-info">By default, this option contains the <b>Order By</b> on the column NewsDate of the News list, which represent an option to insert a Date on a page with an certain order. For more information about this option check the <a href="../../global/publishing">Publishing Settings</a>.</p>

Here you can define what will be the order of your News using the internal name of one of your list colunms name. 

You will need to type in the internal name of the list column on the **Order by** text box - we will sort the News according to the value of the cell name you enter.

Here is what you need to do: 

1. Access the **Teams Settings**

    ![settings_edit.png](../images/msteams/settings_edit.png)

2. Click to Open the **News List**. 

    ![open_list.png](../images/msteams/open_list.png)

2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;

	![internalcolumnname](../images/classic/18.internalcolumnname.png) 
	
6. Now paste the name on the text box;
7. Choose the order, **Ascending** or **Descending**. 

With **Add Order By**, you can add more sorting options. 