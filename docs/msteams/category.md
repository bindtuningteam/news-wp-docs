![category](../images/modern/14.category.png)

### Color Coding

You can organize your news articles by setting color coding on. Colors will be applied based on category.

_____
### Color Coding Settings

You can change colors of the default categories or of your custom categories by using the **Color Coding** option.
For each Category/Color pair, make sure to use the following format:

**Category[#color]**

You can also set the color coding for more than one category using the semicolon to separate them:

**health[#cccccc]; opinions[blue]; customCategory[rgb(0, 0, 0)]**

____
### Custom Icons

You can also organize your news articles by setting custom icons (the layout selected must support icons). The icons will be applied based on category.

____
### Custom Icons Settings

You can also change the icons of the default categories or of your custom categories by using the **Custom Icons** option. For each Category/Icon pair, make sure to use the following format:

**category[icon classes]**

<p class="alert alert-info">The web part only
supports <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank" >Font Awesome icons</a>.</p>

You can set the custom icons for more than one category:

**Politics[fa-briefcase]; tech[fa-gift]; world[fa-cog fa-spin fa-3x fa-fw]; customCategory[fa-cog]**