### Connected with BindTuning News List

1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **News**;

	![add_news](../../images/modern/02.add_news1.gif)

3. Fill out the form that pops up. You can check what you need to do in each section on the [Article Preview](../../global/preview);

4. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more News items with similar configuration. You can also preview the News on the page before saving it, by clicking on the **Preview** button. 

____
### Connected with Custom List


1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **News**;
3. You will be redirect to the List **New Form** page of the Connected List. 
4. After setting everything up, click on **Save** to save the item on the list item.