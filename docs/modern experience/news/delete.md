1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part click the **Manage News** icon;
3. The list of New will appear. Click the **trash** icon to delete the News item;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the News will be removed.	

    ![delete_news](../../images/modern/03.delete_news.gif)