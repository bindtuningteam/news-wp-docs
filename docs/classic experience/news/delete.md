1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage News** icon;
3. The list of New will appear. Click the **trash** icon to delete the News item;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the News will be removed.	

    ![delete_new](../../images/classic/04.delete_new.gif)