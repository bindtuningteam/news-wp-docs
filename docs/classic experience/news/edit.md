### Connected with BindTuning News List

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part News and click the **Manage News** icon;
3. The list of News will appear. Click the **Edit** icon to edit the Item;
4. You can check what you can edit in each section on the [Article Preview](../../global/preview);

	![edit_new](../../images/classic/05.edit_new.gif)

5. Done editing? Click on **Save Changes** to save your settings.

	![save_changes](../../images/classic/13.save_changes.png)

___
### Connected with Custom List

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part News and click the **Manage News** icon;
3. The list of News will appear. Click the **Link External** icon to edit the Item;
4. You will be redirect to the List **Edit Form** page of the Connected List. 