### Connected with BindTuning News List

1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new **News**;

	![add_new](../../images/classic/02.add_new.gif)

4. Fill out the form that pops up. You can check what you need to do in each section on the [Article Preview](../../global/preview);

5. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more News items  with similar configuration. You can also preview the News on the page before saving it, by clicking on the **Preview** button. 

	![save](../../images/classic/16.save.png)

____
### Connected with Custom List

1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new News;
4. You will be redirect to the List **New Form** page of the Connected List. 

	![add_new](../../images/classic/03.add_new.gif)