### Create List

1. Open the settings menu and click on **Add an app**;

	![addanapp.png](../images/classic/17.addanapp.png)

2. On the search box type **BT**;
3. Look for the News list and open it;
4. Pick a name for the app and click on **Create**;

	![bt_list](../images/classic/20.bt_list.png)

5. You've created the list that will contain all the specific fields from this web part.
6. Open the list that you have created, and copy the list URL, you can use it to connect the list to the web part in the next step.

___
### List Settings

Here you will need to connect your web part with a SharePoint list. Without it, the web part will not function and an notification message will appear. Let's get to it:

On **List Settings** you will need to set the lists you want to retrieve your users from.

1. On the first field, place the link to a SharePoint site or list. On the dropdown to the side, you can select a list or one will be selected for you automatically.
2. On **Filtering Options** section, select how you want to filter your items:

    - **No Filters** All the items from your list will be retrieved and displayed on the web part.
    - **CAML Query**: This advanced option gives you full control over how you filter your results. Selecting this option, a text field appears to write your own CAML Query. We recommend the use of the free to use tool <a href="http://www.u2u.be/software/" target="_blank">U2U Caml Query Builder</a>, to help write CAML Queries.
    - **Other Options** : Under the first 2 options, your list views will be listed.

3. On the **Field Mappings** section, map the fields from the list with the fields of the web part:

    - You will find dropdowns for each of the web part's fields. These dropdowns include all the columns in your list's selected view. Select which column corresponds to which field or select **No Mapping** when nothing fits the bill. If you want to repeat the same column for different fields that's ok too.

    ![addanapp.png](../images/classic/25.mappings-card.png)

4. When you're done, lock your selection by clicking the save icon.

____
### How to use CAML Query

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>

<p class="alert alert-info">Use relative paths for this field. So instead of using an URL like <b>https://company.sharepoint.com/sites/Home</b>, you should use something like <b>/sites/Home/</b>. This will ensure that the web part will work regardless of how you’re accessing the site.</p>