![Content](../images/classic/07.content.png)

### Click action

Choose what happens when an article is clicked on. Either **Show Article Content** if a url is provided or **Open Article Site** inside a modal, using the content from the article formatted according to a predefined template.

___
### Show Article Content

![mappings-cotent](../images/classic/26.mappings-content.png)

___
### Open Site in

This option it's only available when for action link you select **Open Article Site**. You choose whether pressing a news item with a link will open this link on a **New Window**, the **Same Window** or a **Modal** (pop-up window) on the page. 

<p class="alert alert-info">Links pointing to external sources might be blocked when using the modal, since the content is displayed inside an iframe. Links pointing to unsecure connections (using http:// instead of https://) will be blocked by SharePoint Online when using the modal.</p>