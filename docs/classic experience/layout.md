![bindtuning-tab](../images/classic/08.layout.png)

### Layout Template

In this setting you can select the styling for your news items. Currently 6 layouts are available. Fields to be displayed in each template are optional, so you can display all fields or just some, according to your needs.

|**Basic**|**Basic Card**|
|----|--|
|![basic_newlayout.png](https://bitbucket.org/repo/oLxq9Gg/images/3750614054-basic_newlayout.png)|![basiccard_newlayout.png](https://bitbucket.org/repo/oLxq9Gg/images/4261686792-basiccard_newlayout.png)|

|**Gradient**|**Gradient Wide**
|--|--|
|![gradient_newlayout.png](https://bitbucket.org/repo/oLxq9Gg/images/2074835639-gradient_newlayout.png)|![gradientwide_newlayout.png](https://bitbucket.org/repo/oLxq9Gg/images/3530251850-gradientwide_newlayout.png)|



|**Boxed**|**Condensed**|
|--|--|
|![boxed_newlayout.png](https://bitbucket.org/repo/oLxq9Gg/images/2335353389-boxed_newlayout.png)|![condensed_newlayout.png](https://bitbucket.org/repo/oLxq9Gg/images/2785038452-condensed_newlayout.png)|

____
### Number of Columns

Here you are able to define the number of columns where your news will be organized, between 1 and 4.

____
### Display Limit

Let’s you limit the amount of news items that appear on the page. By default, the value is 5.