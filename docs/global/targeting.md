## Enable user targeting on News Web Part

<p class="alert alert-info"><b> Note:</b> This feature is available on version 1.8.6.40</p>

After upgrading the web part and edit the page, you should see a warning message on the top of the web part informing that the new feature is available. If you don’t see the warning message, that’s because we can’t enable the feature automatically (it depends on the type of the lists and mappings you’re using).  If you see the message you can activate the feature automatically by clicking “yes”. 
<p>

![News_feature](../images/global/01.news_feature.png)
<p>
By clicking yes, a new column will be created and auto-mapped on all your compatible connected lists. 
At this point, you can define which users can see each article when you create or edit an article.
To do that, just enter users or groups on the Target Users field:

![Publishing_settings](../images/global/02.publishing_settings.png)

___
## Manual configuration to enable user targeting

If you want or need to enable the feature manually, these are the steps: 
<p>

1. To create a TargetUser’s compatible column, visit your connected list:

    ![Cretate_compatible_column](../images/global/03.create_compatible_column.png)

1. Go to Settings &rarr; List Settings:

    ![List_settings](../images/global/04.list_settings.png)

1. Go to Columns &rarr; Create column:

    ![Create_column](../images/global/05.create_column.png)

1. You can choose any name, but we recommend using “TargetUsers” (no spaces) since it will be useful to future auto mapping suggestions. The type must be **Person or Group** and **Allow Multiple selections and Allow selection of People and Groups**:

    ![Configure_column](../images/global/06.configure_column.png)

1. Click **OK** to close. <br>

    At this point, the column is ready to be mapped on your web part’s properties form and you can target users per article.

    ![Configure_target_user](../images/global/07.configure_target_user.png)