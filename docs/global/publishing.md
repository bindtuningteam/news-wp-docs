 ![publishing](../images/classic/23.publishing.png)


### Category

Associate a category with the article. This information is used to format styling, on some templates. 

For more information about styles visit the next link based on your SharePoint:

- [Classic Experience](../classic/category)
- [Modern Experience](../modern/category)
___
**About News Categories and Colors**

When using the BindTuning news list, 8 categories are available. All categories have corresponding colors.

![categories.png](https://bitbucket.org/repo/oLxq9Gg/images/627441601-categories.png)

If you are using your own list, you can map a Categories column when setting up the web part. If a match is foundon the category name, these colors will still be used.
The following category names can be used to match available colors: **Entertainment, Health, Life Style, Opinions, Politics, Science, Sports, Tech, Video, World**

<p class="alert alert-warning">If a news item does not have a matching category, a dark grey color will be used in the layout.</p>

___
### Publication Date

Publication date is displayed in the various news templates. This date is used to order news, most recent news first. Articles with a publication date will only be shown after this date elapses.

_____

To complete the News item, you can configure the other sections that we explain on the link: 

- [Article Cotent](./global/content)
- [Article Preview](./global/preview)

After setting everything up, click on **Save** or **Save and Creat Another**.

![save](../images/classic/16.save.png)