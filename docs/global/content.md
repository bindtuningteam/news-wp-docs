 ![publishing](../images/classic/22.article_content.png)


### Article URL

Here you can set a link to the actual news article. When the news entry is clicked on, this URL will open on a new window or a modal. 

If you select the **Click Action** in the web part properties to **Show article content**, a link to this URL will appear at the end of the article.

____
### Article Image

Here you can set the link to an image, which will be show when the user clicks the news article.

- The image picker, refer to <a href="../image" target="_blank">this section</a>.

<p class="alert alert-info">This option is only visible if the <b>Click Action</b> in the web part properties is set to <b>Show Article Content</b>.</p>

___
### Article Body

<p class="alert alert-info"> This option is only visible if the <b>Click Action</b> in the web part properties is set to <b>Show Article Content</b>.</p>

Here you can set the body that, which will be show when the user clicks the news article. You can use HTML code to enrich the content displayed on the Modal.

____

To complete the News item, you can configure the other sections that we explain on the link:

- [Article Preview](./global/preview)
- [Publishing Settings](./global/publishing)

After setting everything up, click on **Save** or **Save and Creat Another**.

![save](../images/classic/16.save.png)