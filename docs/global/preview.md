 ![preview](../images/classic/21.article_preview.png)

### Title

This is the news title. This will show as an header in the news layouts. This field is limited to 255 characters.
___
### Summary

A short description of the news content. This will show below the title in the news layouts. 

___
### Thumbnail URL

This field will take a URL to the image that is displayed on the news listing.

- The image picker, refer to <a href="../image" target="_blank">this section</a>.

___ 

To complete the News item, you can configure the other sections that we explain on the link: 

- [Article Cotent](./global/content)
- [Publishing Settings](./global/publishing)

After setting everything up, click on **Save** or **Save and Creat Another**.

![save](../images/classic/16.save.png)